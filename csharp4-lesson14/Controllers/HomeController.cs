﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using csharp4_lesson14.Models;

namespace csharp4_lesson14.Controllers
{
    public class HomeController : Controller
    {
        blogcontext b = new blogcontext();
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View(b.blogs.ToList());
        }

    }
}
