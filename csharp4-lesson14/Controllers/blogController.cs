﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using csharp4_lesson14.Models;
using System.Data;

namespace csharp4_lesson14.Controllers
{
    public class blogController : Controller
    {
        blogcontext b = new blogcontext();
        //
        // GET: /blog/

        public ActionResult Index()
        {
            if (b.blogs != null)
            {
                return View("bloglist");
            }
            return View();
        }

        public ActionResult bloglist()
        {
            return View(b.blogs.ToList());
        }

        public ActionResult postlist()
        {
            return View(b.posts.ToList());
        }

        public ActionResult commentlist()
        {
            return View(b.comments.ToList());
        }

        public ActionResult viewblog(int? id)
        {
            return View(b.blogs.Find(id));
        }


        public ActionResult addpost()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult addpost(posts posttocreate)
        {
            if (ModelState.IsValid)
            {
                b.posts.Add(posttocreate);
                b.SaveChanges();
                return RedirectToAction("bloglist");
            }
            return View(posttocreate);
        }

        public ActionResult editpost(int? id)
        {
            posts item = b.posts.Find(id);
            if (item == null)
            {
                return View();
            }
            return View(item);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult editpost(posts posttoedit)
        {
            if (!ModelState.IsValid)
            {
                return View(posttoedit);
            }
            b.Entry(posttoedit).State = EntityState.Modified;
            b.SaveChanges();
            return RedirectToAction("bloglist");
        }

        public ActionResult deletepost(int? id)
        {
            posts item = b.posts.Find(id);
            if (item == null)
            {
                return View();
            }
            b.posts.Remove(item);
            b.SaveChanges();
            return RedirectToAction("bloglist");
        }

        [AcceptVerbs(HttpVerbs.Post), ActionName("deletepost")]
        public ActionResult deletepostcofirmed(int id)
        {
            posts item = b.posts.Find(id);
            b.posts.Remove(item);
            b.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult addblog()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post), ActionName("addblog")]
        public ActionResult addblog([Bind(Exclude = "id")] blog blogtocreate)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            b.blogs.Add(blogtocreate);
            b.SaveChanges();
            return RedirectToAction("bloglist");
        }

        public ActionResult removeblog(int? id)
        {
            blog item = b.blogs.Find(id);
            if (item == null)
            {
                return View("Index");
            }
            b.blogs.Remove(item);
            b.SaveChanges();
            return RedirectToAction("bloglist");
        }

        [AcceptVerbs(HttpVerbs.Post), ActionName("removeblog")]
        public ActionResult removeblogcofirmed(int id)
        {
            blog item = b.blogs.Find(id);
            b.blogs.Remove(item);
            b.SaveChanges();
            return RedirectToAction("bloglist");
        }

        public ActionResult addcomments()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post), ActionName("addcomments")]
        public ActionResult addcomments([Bind(Exclude = "id")] comments commenttocreate)
        {            
            if (!ModelState.IsValid)
            {
                return View();
            }
            b.comments.Add(commenttocreate);
            b.SaveChanges();
            return RedirectToAction("bloglist");   
        }

        public ActionResult removecomments(int? id)
        {
            comments item = b.comments.Find(id);
            b.comments.Remove(item);
            b.SaveChanges();
            return RedirectToAction("bloglist");
        }

        [AcceptVerbs(HttpVerbs.Post), ActionName("removecomments")]
        public ActionResult removecommentscofirmed(int? id)
        {
            comments item = b.comments.Find(id);
            if (item == null)
            {
                return View("Index");
            }
            b.comments.Remove(item);
            b.SaveChanges();
            return RedirectToAction("viewblog");
        }

        public ActionResult getpostsbyblogid(int id)
        {
            var p = b.posts.Where(a => a.fkblog == id);
            return View(p.ToList());
        }

        public ActionResult viewpost(int id)
        {
            return View(b.posts.Find(id));
        }

    }
}
