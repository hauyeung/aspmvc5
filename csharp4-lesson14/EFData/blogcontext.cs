﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using csharp4_lesson14.Models;
using System.Data.Entity.ModelConfiguration.Conventions;
namespace csharp4_lesson14.Models
{
    public class blogcontext: DbContext
    {
        public DbSet<blog> blogs { get; set; }
        public DbSet<posts> posts { get; set; }
        public DbSet<comments> comments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<comments>().HasRequired(g => g.post).WithMany(h => h.comments).HasForeignKey(g => g.fkpost);
            modelBuilder.Entity<posts>().HasRequired(a => a.blog).WithMany(b => b.posts).HasForeignKey(a => a.fkblog);
        }

    }

     
}
