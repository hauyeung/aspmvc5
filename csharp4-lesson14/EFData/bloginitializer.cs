﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using csharp4_lesson14.Models;

namespace csharp4_lesson14.Models
{
    public class bloginitializer : DropCreateDatabaseAlways<blogcontext>
    {
        protected override void Seed(blogcontext context)
        {

            var blogs = new List<blog>
            {
                new blog{title="Blog 1", username = "test", posts = new List<posts>()},
            };
            blogs.ForEach(m => context.blogs.Add(m));
            context.SaveChanges();

            var posts = new List<posts>
            {
                new posts{content = "test content", fkblog=1},
            };
            posts.ForEach(m => context.posts.Add(m));
            context.SaveChanges();

            var comments = new List<comments>
            {
                new comments{content="test comment", fkpost=1},
            };
            comments.ForEach(m => context.comments.Add(m));
            context.SaveChanges();
        }

    }
}
