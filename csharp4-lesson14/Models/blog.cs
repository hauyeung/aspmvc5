﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using csharp4_lesson14.Models;
namespace csharp4_lesson14.Models
{
    public class blog
    {
        [Key]
        public int blogid { get; set; }
        [Display(Name = "Name")]
        public int userid { get; set; }
        public string title { get; set; }
        public string username { get; set; }
        [Display(Name = "Date")]
        public virtual List<posts> posts { get; set; }
    }
}
