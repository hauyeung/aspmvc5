﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using csharp4_lesson14.Models;

namespace csharp4_lesson14.Models
{
    public class comments
    {
        [Key]
        public int id { get; set; }
        public posts post { get; set; }
        [Display(Name = "Content")]
        public string content { get; set; }
        public virtual int fkpost { get; set; }
    }

}
