﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using csharp4_lesson14.Models;

namespace csharp4_lesson14.Models
{
    public class posts
    {
        [Key]
        [Display(Name = "ID")]
        public int id { get; set; }
        public blog blog { get; set; }
        [Display(Name = "Title")]
        public string title { get; set; }
        [Display(Name = "Content")]
        public string content { get; set; }
        public string posteddate = DateTime.Now.ToString();
        public virtual int fkblog { get; set; }
        public virtual List<comments> comments { get; set; }
    }
}
